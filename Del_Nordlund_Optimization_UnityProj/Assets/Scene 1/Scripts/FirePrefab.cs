﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Automatically adds a camera to the gameobjecct this script is attached to. Helps avoid setup errors.
[RequireComponent( typeof( Camera ) )]
public class FirePrefab : MonoBehaviour
{
    public GameObject Prefab;
    public float FireSpeed = 5;

    // Update is called once per frame
    void Update()
    {
        // If button "Fire1" is pressed
        if ( Input.GetButton( "Fire1" ) )
        {
            // Get point in world where player clicked from the camera
            Vector3 clickPoint = GetComponent<Camera>().ScreenToWorldPoint( Input.mousePosition + Vector3.forward );
            // Fire towards point clicked from current position
            Vector3 FireDirection = clickPoint - this.transform.position;
            // Normalize FireDirection
            FireDirection.Normalize();
            // Instantiate a prefab at current position
            GameObject prefabInstance = GameObject.Instantiate( Prefab, this.transform.position, Quaternion.identity, null );
            // Apply velocity to the prefab's rigidbody so it travels in FireDirection at FireSpeed
            prefabInstance.GetComponent<Rigidbody>().velocity = FireDirection * FireSpeed;
        }
    }
}
