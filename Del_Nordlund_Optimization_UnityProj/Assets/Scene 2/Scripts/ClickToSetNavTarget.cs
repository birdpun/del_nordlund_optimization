﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToSetNavTarget : MonoBehaviour
{
    Camera camera;

    private void Awake()
    {
        //Finds main camera in scene
        camera = Camera.main;
    }

    // Update is called once per frame
    void Update ()
    {
        SetDestination();
	}

    void SetDestination()
    {
        if (Input.GetMouseButtonDown(0))
        {
            // Make a raycast hit
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue, LayerMask.GetMask("Ground")))
            {
                // AI player will walk to determined location
                GetComponent<NavMeshAgent>().destination = hit.point;
            }
        }
    }
}
